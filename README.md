###Simple Notes Archiver###

###Introduction###
Objective of this lab is to implement a simple notes archiver.

###Working###
It has two components i.e. Client and Server. Client can upload a note to server and retrieve a note using username. Server listens for a connection and if client sends it a note it simply stores it in memory and if client sends it a username it checks if it has a note against that username, if yes then it returns the notes else it says no note found.

###Implementation###
There are 5 classes a Note class which defines a note structure, a Server class which have server implemented in to store files and retrieve on request, a Client class to send request for upload and download, a Runner class to run Server and a Tester class to perform some test by running Client.
**For testing Runner should be running as a separate process.

** Help is taken form https://systembash.com/a-simple-java-tcp-server-and-tcp-client/ for simple TCP client server functions.
