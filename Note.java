import java.io.*;

public class Note implements Serializable
{
  public String username;
  public String text;
  
  public Note()
  {
    this.username = null;
    this.text = null;
  }
  
  public Note(String username, String text)
  {
    this.username = username;
    this.text = text;
  }
  
  public String to_string()
  {
    return String.join("|||___|||", this.username, this.text);
  }
  
  public void from_string(String raw)
  {
    this.username = raw.substring(0, raw.indexOf("|||___|||"));
    this.text = raw.substring(raw.indexOf("|||___|||") + 9);
  }
}