import java.io.*;
import java.net.*;
import java.util.*;

class Server
{
  HashMap<String, Note> notes;
  int PORT;
  
  public Server()
  {
    this.PORT = 4777;
    notes = new HashMap<String, Note>();
  }
  
  public Server(int PORT)
  {
    this.PORT = PORT;
    notes = new HashMap();
  }
  
   public void handle_requests() throws Exception
   {
         String clientSentence;
         String capitalizedSentence;
         ServerSocket welcomeSocket = new ServerSocket(this.PORT);

         while(true)
         {
            Socket connectionSocket = welcomeSocket.accept();
            BufferedReader inFromClient =
               new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            clientSentence = inFromClient.readLine();
            outToClient.writeBytes(this.process_request(clientSentence) + '\n');
         }
   }
  
  public String process_request(String request)
  {
    String type = request.substring(0, 1);
    request = request.substring(1);
    if(type.equals("1"))
    {
      Note note = new Note();
      note.from_string(request);
      if(store_note(note)){
        return "Stored Successfully!";
      }
      else
        return "Some error occured while storing";
    }
    else
    {
      Note note = get_note(request);
      String resp = "";
      if(note == null)
        resp = "No record found";
      else
        resp = note.to_string();
      return resp;
    }
  }
  
  public Note get_note(String username)
  {
    if(this.notes.containsKey(username))
      return this.notes.get(username);
    else
      return null;
  }
  
  public boolean store_note(Note note)
  {
    this.notes.put(note.username, note);
    return true;
  }
}