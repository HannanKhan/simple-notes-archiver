import java.io.*;
import java.net.*;

public class Client
{
  public String IP;
  public int PORT;
  
  public Client()
  {
    IP = "localhost";
    PORT = 4777;
  }
  
  public Client(String IP, int PORT)
  {
    this.IP = IP;
    this.PORT = PORT;
  }
  
  public void upload_note(Note note) throws Exception
  {
    String sentence;
    String modifiedSentence;
    Socket clientSocket = new Socket(this.IP, this.PORT);
    DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    sentence = "1" + note.to_string();
    outToServer.writeBytes(sentence + '\n');
    modifiedSentence = inFromServer.readLine();
    System.out.println(modifiedSentence);
    clientSocket.close();
  }
  
  public Note get_note(String username) throws Exception
  {
    String sentence;
    String modifiedSentence;
    Socket clientSocket = new Socket(this.IP, this.PORT);
    DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    sentence = "2" + username;
    outToServer.writeBytes(sentence + '\n');
    modifiedSentence = inFromServer.readLine();
    clientSocket.close();
    Note note = new Note();
    note.from_string(modifiedSentence);
    return note;
  }
}