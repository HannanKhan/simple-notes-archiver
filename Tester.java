class Tester
{

  public static void main(String argv[]) throws Exception
  {
    Tester.test_object_reconstruction();
    Tester.test_record_and_retrieval();
  }
  
  public static void test_object_reconstruction() throws Exception
  {
    System.out.println("\t\t\tTesting Object Reconstruction\n");
    System.out.println("Creating note of user : bot, text : Just simple lab note!\n");
    
    Note note = new Note("bot", "Just simple lab note!");
    
    System.out.println("Reconstructing note...\n");
    
    Note r_note = new Note();
    r_note.from_string(note.to_string());
    
    System.out.println("Reconstructed note -> user : " + r_note.username + ", text : " + r_note.text + "\n");
    
    if(r_note.username.equals(note.username) && r_note.text.equals(note.text))
      System.out.println("Test Passed!\n");
    else
      System.out.println("Test Failed!\n\n");
  }
  
  public static void test_record_and_retrieval () throws Exception
  {
    System.out.println("\t\t\tRunning Echo Test\n");
    System.out.println("Sending note \'Doing Lab!!!\'\n");
    
    Client client = new Client();
    Note note = new Note("me","Doing Lab!!!");
    client.upload_note(note);
    
    Note result_note = client.get_note("me");
    
    System.out.println("fetched note \'" + result_note.text + "\'\n");
    
    if(note.text.equals(result_note.text))
      System.out.println("Test Passed!\n");
    else
      System.out.println("Test Failed!\n\n");
  }
}